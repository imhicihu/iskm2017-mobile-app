## Tools
* [International Accent Marks and Diacriticals](http://www.starr.net/is/type/htmlcodes.html): best practice as possible between Google indexing, page rendering, _etc_.

## To explore
* https://expo.io/@react-navigation/NavigationPlayground
* https://webpack.js.org/