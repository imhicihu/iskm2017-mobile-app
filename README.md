![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# Rationale #

* Workflow / Seminal Project: 
     - A native, mobile and the official app related with the [ISKM 2017 Symposium](http://www.imhicihu-conicet.gob.ar/iskm2017/) celebrated on Buenos Aires, Argentina, on november 2017.

![iskm2017-web.png](https://bitbucket.org/repo/bBMkd4/images/892361199-iskm2017-web.png)

### What is this repository for? ###

* ~~Hybrid Web-App - App -~~ Native Android App
* Version 1.1. [No planned updates](https://bitbucket.org/imhicihu/iskm2017-mobile-app/issues/17/workflow-update-security-updates-planned).

### How do I get set up? ###

* Summary of set up
     - Lots of tools and the interaction between all of them. Implies hardware (real and virtual) and software (mostly open-source)
* Configuration
     - [Android Studio](https://developer.android.com/studio/index.html?hl=es-419)
     - [Java](https://www.java.com/)
     - [Docker](https://www.docker.com/)
     - [Android File Transfer](https://www.android.com/filetransfer/) 
     - mobile emulators ([Bluestack](https://www.bluestacks.com/es/index.html), _mainly_) 
     - [Zotero](https://www.zotero.org/)
     - [Frameworks](https://bitbucket.org/imhicihu/iskm2017-mobile-app/issues/edit/15)
     - and a myriad of more tools & online services...

* Dependencies
     - [NPM](https://www.npmjs.com/)
     - [Bower](https://bower.io/)
* How to run the code
     - Go to the Play Store. [Download for free](https://play.google.com/store/apps/details?id=com.iskm2017.app_120833_124594&hl=es).    
      ![4097326788-iskm2017.png](https://bitbucket.org/repo/Gg8Xdg7/images/394890652-4097326788-iskm2017.png)

* Deployment instructions
     - ~~Download from this repo or our _Github_ account the .IPA or .APK. Then~~ [Install it](https://play.google.com/store/apps/details?id=com.iskm2017.app_120833_124594&hl=es) on your mobile device. Run it.    

### Related repositories ###

* Some repositories linked with this project:
     - [Bibliographical hybrid (mobile app)](https://bitbucket.org/imhicihu/bibliographical-hybrid-mobile-app/src/)
     - [NPM under proxy (settings)](https://bitbucket.org/imhicihu/npm-under-proxy-settings/src/master/)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/iskm2017-mobile-app/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
     - Contact at `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/iskm2017-mobile-app/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account).

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/iskm2017-mobile-app/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners. 